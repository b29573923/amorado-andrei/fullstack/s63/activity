function countLetter(letter, sentence) {
    if (typeof letter !== 'string' || letter.length !== 1) {
        return undefined;
    }

    let count = 0;
    for (let i = 0; i < sentence.length; i++) {
        if (sentence[i] === letter) {
            count++;
        }
    }

    return count;
}

function isIsogram(text) {
    const lowercasedText = text.toLowerCase();

    const charSet = new Set();


    for (let i = 0; i < lowercasedText.length; i++) {
        const char = lowercasedText[i];

        if (charSet.has(char)) {
            return false;
        }

        charSet.add(char);
    }

    return true;
}

function purchase(age, price) {
    if(age <13) {
        return undefined;
    }

    let discountPrice = (price * 0.8).toFixed(2);

    if (age >= 13 && age <=21) {
        discountPrice = price * 0.8;
    } else if (age >= 65) {
        discountPrice = price * 0.8;
    } else {
        discountPrice = price;
    }

    return discountPrice.toFixed(2);
};


const items = [
  { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
  { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
  { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
  { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
  { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
];


function findHotCategories(items) {

  const hotCategories = [];
  const uniqueCategories = new Set();

  for (const item of items) {
    if (item.stocks === 0 && !uniqueCategories.has(item.category)) {
      hotCategories.push(item.category);
      uniqueCategories.add(item.category);
    }
  }

  return hotCategories;
}

const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];



function findFlyingVoters(candidateA, candidateB) {

  const commonVoters = candidateA.filter(voter => candidateB.includes(voter));

  return commonVoters;
    
}
const result = findFlyingVoters(candidateA, candidateB);
console.log(result);


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};